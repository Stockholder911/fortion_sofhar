﻿using System;
using Xamarin.Forms;
using System.IO;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Foundation;

[assembly: Dependency(typeof(FortiON.App.WinPhone.SaveAndLoad))]
namespace FortiON.App.WinPhone
{
	public class SaveAndLoad : ISaveAndLoad {

        public string LoadText(string filename)
        {
            var task = LoadTextAsync(filename);
            task.Wait();
            return task.Result;
        }

        
        async Task<string> LoadTextAsync(string filename)
        {
            StorageFolder local = Windows.Storage.ApplicationData.Current.LocalFolder;
            if (local != null)
            {
                Windows.Storage.IStorageItem file = null;

                try
                {
                    file = await local.GetItemAsync(filename);
                }
                catch (System.IO.FileNotFoundException e)
                {
                }

                if (file != null)
                {
                    using (StreamReader streamReader = new StreamReader(file.Path))
                    {
                        var text = streamReader.ReadToEnd();
                        return text;
                    }
                }
                
            }
            return "";
        }
        
/*
        public void SaveText(string filename, string text)
        {
            StorageFolder local = Windows.Storage.ApplicationData.Current.LocalFolder;
            var file = local.CreateFileAsync(filename, CreationCollisionOption.ReplaceExisting).GetResults();

            using (StreamWriter writer = new StreamWriter(file.OpenStreamForWriteAsync().Result))
            {
                writer.Write(text);
            }
        }
 */

        public async void SaveText(string filename, string text)
        {
            var task = SaveTextAsync(filename, text);
            await task;
        }

        async Task SaveTextAsync(string filename, string text)
        {
            StorageFolder local = Windows.Storage.ApplicationData.Current.LocalFolder;
            var file = await local.CreateFileAsync(filename, CreationCollisionOption.ReplaceExisting);

            using (StreamWriter writer = new StreamWriter(await file.OpenStreamForWriteAsync()))
            {
                writer.Write(text);
            }
        }

	}
}

