﻿using System;
using Xamarin.Forms;
using Microsoft.Phone.Net.NetworkInformation;

[assembly: Dependency(typeof(FortiON.App.WinPhone.Network))]
namespace FortiON.App.WinPhone
{
	public class Network : INetwork
	{
		public bool IsNetworkConnected(){

            return DeviceNetworkInformation.IsNetworkAvailable;
		}
	}
}

