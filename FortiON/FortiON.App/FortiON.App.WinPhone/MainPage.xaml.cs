﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Windows.Devices.Geolocation;
using System.Threading;

namespace FortiON.App.WinPhone
{
    public partial class MainPage : global::Xamarin.Forms.Platform.WinPhone.FormsApplicationPage
    {
        Geolocator myGeoLocator;

        static int TimerTimeout = 60 * 1000;

        public MainPage()
        {
            try
            {
                myGeoLocator = new Geolocator();
                myGeoLocator.DesiredAccuracy = PositionAccuracy.Default;
                myGeoLocator.MovementThreshold = 50;
                myGeoLocator.StatusChanged += myGeoLocator_StatusChanged;
                myGeoLocator.PositionChanged += myGeoLocator_PositionChanged;
                PositionStatus gpsStatus = myGeoLocator.LocationStatus;
                UpdateGpsStatus(gpsStatus);


                TimerCallback timerDelegate = new TimerCallback(Tick);
                Timer timer = new Timer(timerDelegate, null, TimerTimeout, TimerTimeout);

                InitializeComponent();
                SupportedOrientations = SupportedPageOrientation.PortraitOrLandscape;

                global::Xamarin.Forms.Forms.Init();
                LoadApplication(new FortiON.App.App());
            }
            catch (Exception ex) { }
        }

        static void Tick(Object state)
        {
            TimerUpdate.Tick();
        }

        void UpdateGpsStatus(PositionStatus gpsStatus)
        {
            try
            {
                if (gpsStatus == PositionStatus.Ready)
                {
                    Gps.IsEnabled = true;
                    Gps.Availability = GpsAvailability.Available;
                }
                else
                {
                    Gps.Availability = GpsAvailability.TemporarilyUnavailable;
                    if (gpsStatus == PositionStatus.Disabled)
                        Gps.IsEnabled = false;
                }
                Gps.OnUpdateGps();
            }
            catch (Exception ex) { }
        }

        void myGeoLocator_StatusChanged(Geolocator sender, StatusChangedEventArgs args)
        {
            UpdateGpsStatus(args.Status);
        }

        void myGeoLocator_PositionChanged(Geolocator sender, PositionChangedEventArgs args)
        {
            Gps.Latitude = args.Position.Coordinate.Latitude;
            Gps.Longitude = args.Position.Coordinate.Longitude;
            Gps.OnUpdateGps();
        }
    }
}
