﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ServiceModel;
using FortiON.App.DataAccess.ServiceReference;

namespace FortiON.App.DataAccess
{
    public class DaoUtil
    {
        public static readonly EndpointAddress EndPoint = new EndpointAddress("http://186.215.184.177:8484/FortiONMobileService.svc");

        public DaoUtil()
        {

        }

        public static FortiONMobileServiceClient GetClient(TimeSpan timeout)
        {
            return new FortiONMobileServiceClient(CreateBasicHttp(timeout), EndPoint);
        }

        public static FortiONMobileServiceClient GetClient()
        {
            TimeSpan timeout = new TimeSpan(0, 1, 30);
            return GetClient(timeout);
        }

        public static BasicHttpBinding CreateBasicHttp(TimeSpan timeout)
        {
            BasicHttpBinding binding = new BasicHttpBinding
            {
                Name = "basicHttpBinding",
                MaxBufferSize = 2147483647,
                MaxReceivedMessageSize = 2147483647
            };
            binding.SendTimeout = timeout;
            binding.OpenTimeout = timeout;
            binding.ReceiveTimeout = timeout;
            return binding;
        }
    }
}

