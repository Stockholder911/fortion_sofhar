﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Net;
using FortiON.App.DataAccess.ServiceReference;
using Newtonsoft.Json;

namespace FortiON.App
{
	public class Util
	{
		public static Color FortiOnBlue = Color.FromHex("01456F");
		public static Color FortionGray = Color.FromHex("E2E2E2");
		public static Color FortiOnItemSelected = Color.FromHex("75C5F0");
		public static Color FortiOnItemNoSelected = Color.Black;

		private static string Token = null;

		private static string TokenFileName = "token.txt";
		private static string NotificacaoFileName = "notificacao.txt";
		private static string EmbarqueFileName = "embarque.txt";

		public static NotificacaoEntidade NotificacaoAtiva;

        private static DadosEmbarque dadosEmbarque;
        private static bool isEmbarqueLoaded = false;
        private static List<NotificacaoEntidade> listaNotificacoes;

		public Util ()
		{
		}	

		public async static void SaveToken(string token)
		{
            Token = token;
			DependencyService.Get<ISaveAndLoad>().SaveText(TokenFileName, token);
		}

		public static string LoadToken()
		{
			return DependencyService.Get<ISaveAndLoad>().LoadText(TokenFileName);
		}

		public static string GetToken(){
			if(Token == null)
				Token = LoadToken();
			return Token;
		}

        static List<NotificacaoEntidade> OrdenaLista(List<NotificacaoEntidade> lista)
        {

            lista.Sort((x, y) => string.Compare(x.Id_Origem.ToString(), y.Id_Origem.ToString()));

            List<NotificacaoEntidade> listaOrdenadaSeq = lista;
            listaOrdenadaSeq.Sort((x, y) => string.Compare(x.Sequencia.ToString(), y.Sequencia.ToString()));

            List<NotificacaoEntidade> listaOrdenadaId = lista;
            listaOrdenadaSeq.Sort((x, y) => string.Compare(x.Id.ToString(), y.Id.ToString()));

            List<NotificacaoEntidade> listaOrdenada = new List<NotificacaoEntidade>();

            foreach (NotificacaoEntidade e in listaOrdenadaId)
            {
                if (e.Id_Origem == -99 || e.Id_Origem == 0)
                    listaOrdenada.Add(e);
            }

            foreach (NotificacaoEntidade e in listaOrdenadaId)
            {
                if (e.Id_Origem != -99 && e.Id_Origem != 0)
                {
                    if (!listaOrdenada.Contains(e))
                        listaOrdenada.Add(e);
                    foreach (NotificacaoEntidade e2 in listaOrdenadaSeq)
                    {
                        if (!listaOrdenada.Contains(e2))
                        {
                            if (e2.Id_Origem == e.Id)
                                listaOrdenada.Add(e2);
                        }
                    }
                }
            }
            return listaOrdenada;
        }

		public static void SaveNotificacao(List<NotificacaoEntidade> lista){

            listaNotificacoes = (lista != null)?OrdenaLista(lista):null;
            var json = JsonConvert.SerializeObject(listaNotificacoes);
            DependencyService.Get<ISaveAndLoad>().SaveText(NotificacaoFileName, json);
		}

		public static List<NotificacaoEntidade> LoadNotificacao(){

            string json = DependencyService.Get<ISaveAndLoad>().LoadText(NotificacaoFileName);
            if (json != string.Empty)
            {
                listaNotificacoes = JsonConvert.DeserializeObject<List<NotificacaoEntidade>>(json);
            }

            return listaNotificacoes;
		}

        public static void SaveDadosEmbarque(DadosEmbarque e)
        {
            if (e != null)
            {
                var json = JsonConvert.SerializeObject(e);
                DependencyService.Get<ISaveAndLoad>().SaveText(EmbarqueFileName, json);
                dadosEmbarque = e;
            }
        }

        public static DadosEmbarque LoadDadosEmbarque()
        {
            if (isEmbarqueLoaded)
            {
                return dadosEmbarque;
            }

            string json = DependencyService.Get<ISaveAndLoad>().LoadText(EmbarqueFileName);
            if (json != string.Empty)
            {
                dadosEmbarque = JsonConvert.DeserializeObject<DadosEmbarque>(json);
            }

            isEmbarqueLoaded = true;

            return dadosEmbarque;
        }

		public static bool IsNetworkConnected(){
			return DependencyService.Get<INetwork>().IsNetworkConnected();
		}

	}
}

