﻿using System;
using Xamarin.Forms;

namespace FortiON.App
{
	class NotificacaoCell : ViewCell
	{
		public NotificacaoCell()
		{
			var image = new Image
			{
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Center,
				BackgroundColor = Util.FortiOnBlue
			};
			image.SetBinding(Image.SourceProperty, new Binding("ImageUri"));
			image.WidthRequest = image.HeightRequest = 20;

			var lblTitulo = new Label
			{
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.Center,
				TextColor = Color.Black,
				FontAttributes = FontAttributes.Bold
			};
			lblTitulo.SetBinding(Label.TextProperty, "Titulo");

			var lblTexto = new Label
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				TextColor = Color.Black
			};
			lblTexto.SetBinding(Label.TextProperty, "Texto");

			var tituloLayout = new StackLayout()
			{
				Orientation = StackOrientation.Horizontal,
				Children = { image, lblTitulo }
			};

			var mainLayout = new StackLayout()
			{
				Orientation = StackOrientation.Vertical,
				Children = { tituloLayout, lblTexto },
				Padding = new Thickness(20, 10, 20, 20)
			};

			var viewLayout = new StackLayout()
			{
				Orientation = StackOrientation.Vertical,
				Children = { mainLayout },
				BackgroundColor = Util.FortionGray,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};

			viewLayout.SetBinding( Layout.BackgroundColorProperty, new Binding( "BackgroundColor" ) );

			View = viewLayout;
		}
	}
}

