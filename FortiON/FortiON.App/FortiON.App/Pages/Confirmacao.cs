﻿using System;
using Xamarin.Forms;
using System.Threading;
using FortiON.App.DataAccess;
using FortiON.App.DataAccess.ServiceReference;

namespace FortiON.App
{
	public class Confirmacao : ContentPage
	{
		Menu Menu = new Menu();
		double SliderMin = 0;
		double SliderMax = 10;

		Slider Slider;
		bool IsJaInformouEntrega = false;
		bool IsSaiuPaginaAsync = false;

		public Confirmacao()
		{
			try{
				this.Title = "CONFIRMAÇÃO";

				StackLayout layoutMain = new StackLayout {
					Orientation = StackOrientation.Vertical,
					BackgroundColor = Color.White,
					VerticalOptions = LayoutOptions.FillAndExpand
				};

				StackLayout layoutCima = new StackLayout {
					Orientation = StackOrientation.Vertical,
					BackgroundColor = Color.White,
					VerticalOptions = LayoutOptions.FillAndExpand,
					Padding = new Thickness (20, 40, 20, 20)
				};

				StackLayout layoutLabels = new StackLayout {
					Orientation = StackOrientation.Horizontal,
					BackgroundColor = Color.White,
					VerticalOptions = LayoutOptions.Start,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					Padding = new Thickness (40, 40, 40, 10)
				};

				StackLayout layoutSlider = new StackLayout {
					Orientation = StackOrientation.Vertical,
					VerticalOptions = LayoutOptions.Start,
					Padding = new Thickness (0, 0, 0, 45)
				};

				StackLayout layoutBaixo = new StackLayout {
					Orientation = StackOrientation.Vertical,
					BackgroundColor = Color.White,
					VerticalOptions = LayoutOptions.End,
				};

				Label lblMsg = new Label {
					Text = "Você tem certeza de que deseja confirmar a entrega desta carga? Após confirmação, não será possível retornar às informações anteriores.\nSe deseja confirmar, arraste o ícone até o \"SIM\".",
					TextColor = Color.Black
				};

				Slider = new Slider{
					BackgroundColor = Util.FortionGray,
					Minimum = SliderMin,
					Maximum = SliderMax,
					VerticalOptions = LayoutOptions.Start
				};
				Slider.ValueChanged += SliderValueChange;

				Label lblNao = new Label {
					Text = "NÃO",
					TextColor = Color.Black,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					XAlign = TextAlignment.Start
				};

				Label lblSim = new Label {
					Text = "SIM",
					TextColor = Color.Black,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					XAlign = TextAlignment.End
				};

				Button btnVoltar = new Button
				{
					Text = String.Format("Voltar          "),
					BackgroundColor = Util.FortiOnBlue,
					TextColor = Color.White,
					BorderRadius = 0,
					VerticalOptions = LayoutOptions.EndAndExpand
				};
				btnVoltar.Clicked += (sender, args) =>
				{
                    Navigation.PushAsync(new Principal());
				};

				Menu.CreateToolBar (this);


				btnVoltar.Image = (FileImageSource) ImageSource.FromFile ("drawable/Voltar.png");;

				layoutLabels.Children.Add (lblNao);
				layoutLabels.Children.Add (lblSim);

				layoutSlider.Children.Add (Slider);

				layoutCima.Children.Add (lblMsg);
				layoutBaixo.Children.Add (btnVoltar);
				layoutMain.Children.Add (layoutCima);
				layoutMain.Children.Add (layoutLabels);
				layoutMain.Children.Add (layoutSlider);
				layoutMain.Children.Add (layoutBaixo);

				this.Content = layoutMain;
			}
			catch(Exception ex){
				DisplayAlert ("Fortion", "OCORREU UM PROBLEMA: " + ex.Message, "OK");
			}
		}	

		private void SliderValueChange (object sender, ValueChangedEventArgs e)
		{

           // Device.BeginInvokeOnMainThread(() =>
           // {
			    try{
				    if (!IsJaInformouEntrega && (int)e.NewValue == (((int)SliderMax - 1))) {
					    IsJaInformouEntrega = true;
					    Slider.Value = 0;
                        if (Util.IsNetworkConnected())
                        {
                            FortiONMobileServiceClient c = DaoUtil.GetClient();
                            DadosTransmissao dadosTransmissao = new DadosTransmissao();
                            dadosTransmissao.VeiculoLat = Gps.Latitude;
                            dadosTransmissao.VeiculoLng = Gps.Longitude;

                            c.ConfirmarEntregaCompleted += (send, args) =>
                            {
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    if (args.Result)
                                    {
                                        DisplayAlert("Fortion", "SUA VIAGEM FOI CONCLUÍDA!", "OK");
                                    }
                                    else
                                    {
                                        DisplayAlert("Fortion", "NÃO FOI POSSÍVEL ENVIAR A CONFIRMAÇÃO", "OK");
                                    }

                                    SaiPaginaAsync();
                                });
                            };

                            c.ConfirmarEntregaAsync(Util.GetToken(), dadosTransmissao);
                        }
                        else
                        {
                            DisplayAlert("Fortion", "NÃO HÁ CONEXÃO COM A INTERNET", "OK");
                            Navigation.PushAsync(new Principal());
                        }
				    }
			    }
			    catch(Exception ex){
				    DisplayAlert ("Fortion", "OCORREU UM PROBLEMA: " + ex.Message, "OK");
                    Navigation.PushAsync(new Principal());
			    }
           // }); 
		}

		private void SaiPaginaAsync()
		{
			//@TH - apagando os dados locais para forçar buscar no webservice os dados novamente, para evitar o problema do webservice não responder e mesmoa ssim fazer o registro da entrega
			if(!IsSaiuPaginaAsync){
                Navigation.PushAsync(new Principal());
				IsSaiuPaginaAsync = true;
			}
		}
	}
}

