﻿using System;
using Xamarin.Forms;
using System.Threading;
using FortiON.App.DataAccess;
using System.ServiceModel;
using System.Collections.Generic;
using FortiON.App.DataAccess.ServiceReference;
using System.Threading.Tasks;

namespace FortiON.App
{
    public class Principal : ContentPage, IGps, ITimerUpdate
	{

		Menu Menu = new Menu();
		Image ImgGps = new Image
		{
			HorizontalOptions = LayoutOptions.CenterAndExpand,
			VerticalOptions = LayoutOptions.Center,
			Source = "drawable/GPS_On.png"
		};

		Image imgNetwork;

		Label lblOrigem;
		Label lblDestino;
		Button btnNotificacao;

		string StrNovaNotificacao = " (*)";

		bool JaExibiuUmaVez = false;

        string oldNumeroEmbarque;


		public Principal()
		{
			try{

				this.Title = "FORTION";

				StackLayout layoutMain = new StackLayout {
					Orientation = StackOrientation.Vertical,
					BackgroundColor = Color.White,
					VerticalOptions = LayoutOptions.FillAndExpand
				};

				StackLayout layoutCima = new StackLayout {
					Orientation = StackOrientation.Vertical,
					BackgroundColor = Color.White,
					VerticalOptions = LayoutOptions.FillAndExpand,
					Padding = new Thickness (0, 10, 0, 0)
				};

				StackLayout layoutStatus = new StackLayout {
					Orientation = StackOrientation.Horizontal,
					BackgroundColor = Util.FortionGray,
					VerticalOptions = LayoutOptions.Start,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					Padding = new Thickness (0, 4, 0, 4),
					HeightRequest = 40
				};

				StackLayout layoutGps = new StackLayout {
					Orientation = StackOrientation.Horizontal,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					Padding = new Thickness (0, 0, 1, 0)
				};

				StackLayout layoutEmbarque = new StackLayout {
					Orientation = StackOrientation.Vertical,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					VerticalOptions = LayoutOptions.FillAndExpand,
					BackgroundColor = Util.FortionGray,
					Padding = new Thickness (0, 20, 0, 0)
				};

				StackLayout layoutEmbarqueConteudo = new StackLayout {
					Orientation = StackOrientation.Vertical,
					HorizontalOptions = LayoutOptions.FillAndExpand
				};

				ScrollView layoutScroll = new ScrollView  {
					Orientation = ScrollOrientation.Vertical,
					VerticalOptions = LayoutOptions.FillAndExpand
				};

				Label lblEmbarque = new Label
				{	
					HorizontalOptions = LayoutOptions.Center,
					VerticalOptions = LayoutOptions.Center,
					Text = "EMBARQUE ATUAL"
				};

				lblOrigem = new Label
				{
					HorizontalOptions = LayoutOptions.Start,
					VerticalOptions = LayoutOptions.Center,
					Text = "Origem:"
				};

				lblDestino = new Label
				{
					HorizontalOptions = LayoutOptions.Start,
					VerticalOptions = LayoutOptions.Center,
					Text = "Destino:"
				};

				layoutScroll.Content = layoutEmbarqueConteudo;

				Label lblGps = new Label
				{
					HorizontalOptions = LayoutOptions.CenterAndExpand,
					VerticalOptions = LayoutOptions.Center,
					Text = "GPS"
				};

				StackLayout layoutNetwork = new StackLayout {
					Orientation = StackOrientation.Horizontal,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					Padding = new Thickness (0, 0, 1, 0)
				};

				imgNetwork = new Image
				{
					HorizontalOptions = LayoutOptions.CenterAndExpand,
					VerticalOptions = LayoutOptions.Center
				};

				Label lblNetwork = new Label
				{
					HorizontalOptions = LayoutOptions.CenterAndExpand,
					VerticalOptions = LayoutOptions.Center,
					Text = "Internet"
				};

				layoutGps.Children.Add (ImgGps);
				layoutGps.Children.Add (lblGps);
				layoutNetwork.Children.Add (imgNetwork);
				layoutNetwork.Children.Add (lblNetwork);

				StackLayout layoutBaixo = new StackLayout {
					Orientation = StackOrientation.Vertical,
					BackgroundColor = Color.White,
					VerticalOptions = LayoutOptions.End
				};	

				btnNotificacao = new Button
				{
					Text = String.Format("Notificação          "),
					BackgroundColor = Util.FortiOnBlue,
					TextColor = Color.White,
					BorderRadius = 0,
					VerticalOptions = LayoutOptions.Start
				};
				btnNotificacao.Clicked += (sender, args) =>
				{
                   Device.BeginInvokeOnMainThread(() => {
                       Navigation.PushAsync(new Notificacao());
                   });
				};

                LoadDadosEmbarqueFromServer(Util.LoadDadosEmbarque());

				Button btnInformar = new Button
				{
					Text = String.Format("Informar entrega"),
					BackgroundColor = Util.FortiOnBlue,
					TextColor = Color.White,
					BorderRadius = 0,
					VerticalOptions = LayoutOptions.EndAndExpand
				};
				btnInformar.Clicked += (sender, args) =>
				{
                    Device.BeginInvokeOnMainThread(() => {
                        if (Util.LoadDadosEmbarque() != null)
                            Navigation.PushAsync(new Confirmacao());
                        else
                            DisplayAlert("Fortion", "NENHUM EMBARQUE", "OK");
                    });
				};

				Button btnProblema = new Button
				{
					Text = String.Format("Estou com problema"),
					BackgroundColor = Util.FortiOnBlue,
					TextColor = Color.White,
					BorderRadius = 0,
					VerticalOptions = LayoutOptions.EndAndExpand
				};
				btnProblema.Clicked += (sender, args) =>
				{
                    Device.BeginInvokeOnMainThread(() => {
                        btnNotificacao.Text = btnNotificacao.Text.Replace(StrNovaNotificacao, string.Empty);
                        Navigation.PushAsync(new EstouComProblema());
                    });
				};

				Menu.CreateToolBar(this);
					
				if(Util.IsNetworkConnected())
					imgNetwork.Source = "drawable/Wifi_On.png";
				else
					imgNetwork.Source = "drawable/Wifi_Off.png";

				btnNotificacao.Image = (FileImageSource) ImageSource.FromFile ("drawable/Bandeira.png");
				btnInformar.Image = (FileImageSource) ImageSource.FromFile ("drawable/Caminhao.png");
				btnProblema.Image = (FileImageSource) ImageSource.FromFile ("drawable/Problema.png");

				layoutEmbarqueConteudo.Children.Add (lblOrigem);
				layoutEmbarqueConteudo.Children.Add (lblDestino);
				layoutEmbarque.Children.Add (lblEmbarque);
				layoutEmbarque.Children.Add (layoutScroll);
				layoutStatus.Children.Add (layoutGps);
				layoutStatus.Children.Add (layoutNetwork);
				layoutCima.Children.Add (layoutStatus);
				layoutCima.Children.Add (btnNotificacao);
				layoutCima.Children.Add (layoutEmbarque);
				layoutBaixo.Children.Add (btnInformar);
				layoutBaixo.Children.Add (btnProblema);
				layoutMain.Children.Add (layoutCima);
				layoutMain.Children.Add (layoutBaixo);

				Gps.Listener = this;
				TimerUpdate.Listener = this;

				this.Content = layoutMain;

                if (string.IsNullOrEmpty(Util.GetToken()))
                {
                    Navigation.PushAsync(new Configuracao());
					return;
				}
			}
			catch(Exception ex){
				DisplayAlert ("Fortion", "OCORREU UM PROBLEMA: " + ex.Message, "OK");
			}
		}

        void LoadDadosEmbarqueFromServer(DadosEmbarque e)
        {
			try{
				if (Util.IsNetworkConnected()) {

                    if (string.IsNullOrEmpty(Util.GetToken()))
                    {
                        return;
                    }

					oldNumeroEmbarque = string.Empty;
					if(e != null)
						oldNumeroEmbarque = e.NumeroEmbarque;
					FortiONMobileServiceClient c = DaoUtil.GetClient ();
                    c.GetDadosEmbarqueCompleted += getDadosEmbarqueCompleted;
                    c.GetDadosEmbarqueAsync(Util.GetToken());
				} else {
					DisplayAlert ("Fortion", "NÃO HÁ CONEXÃO COM A INTERNET", "OK");
				}
			}
			catch(Exception ex){
				DisplayAlert ("Fortion", "OCORREU UM PROBLEMA: " + ex.Message, "OK");
			}
		}

        private void getDadosEmbarqueCompleted(object sender, GetDadosEmbarqueCompletedEventArgs e)
        {
             Device.BeginInvokeOnMainThread(() => {
                if (e.Result == null)
                {
                    DisplayAlert("Fortion", "NENHUM EMBARQUE", "OK");
                    return;
                }

                if (e.Result != null && e.Result.NumeroEmbarque != oldNumeroEmbarque)
                {
                    DisplayAlert("Fortion", "NOVO EMBARQUE", "OK");
                    Util.SaveDadosEmbarque(e.Result);
                }

                ExibeDadosEmbarque(e.Result);
             });
        }

        void ExibeDadosEmbarque(DadosEmbarque e)
        {
			if (e != null) {
				lblOrigem.Text = "Origem:\nEstado: " + e.EstadoOrigem + "\nCidade: " + e.CidadeOrigem + "\nCEP: " + e.CEPOrigem + "\nLogradouro: " + e.LogradouroOrigem + "\nBairro: " + e.BairroOrigem + "\nNúmero: " + e.NumeroOrigem + "\nComplemento: " + e.ComplementoOrigem + "\nPaís: " + e.PaisOrigem + "\n";
				lblDestino.Text = "Destino:\nEstado: " + e.EstadoDestino + "\nCidade: " + e.CidadeDestino + "\nCEP: " + e.CEPDestino + "\nLogradouro: " + e.LogradouroDestino + "\nBairro: " + e.BairroDestino + "\nNúmero: " + e.NumeroDestino + "\nComplemento: " + e.ComplementoDestino + "\nPaís: " + e.PaisDestino + "\n";
				lblDestino.Text += "\n\n CNPJ Transportadora: " + e.CNPJTransportadora + "\n Data Embarque: " + e.DataEmbarque + "\n Descricao Produto: " + e.DescricaoProduto + "\n Nome Motorista: " + e.NomeMotorista + "\n Nota Fiscal: " + e.NotaFiscal + "\n Número Contrato: " + e.NumeroContrato + "\n Número Embarque: " + e.NumeroEmbarque + "\n Placa Veículo: " + e.PlacaVeiculo + "\n Produto: " + e.Produto + "\n Quantidade: " + e.Quantidade + "\n Razao Social Transportadora: " + e.RazaoSocialTransportadora + "\n Telefone Motorista: " + e.TelefoneMotorista + "\n Valor Frete: " + e.ValorFrete + "\n Valor Pedágio: " + e.ValorPedagio + "\n Valor Tonelada: " + e.ValorTonelada + "\n";
			} else {
				lblOrigem.Text = "Origem:";
				lblDestino.Text = "Destino:";
			}
		}

		protected override void OnAppearing (){
			Device.BeginInvokeOnMainThread (() => {
				try{
					//DisplayAlert ("Fortion", "OnAppearing" + ex.Message, "OK");
					btnNotificacao.Text = btnNotificacao.Text.Replace(StrNovaNotificacao, string.Empty);
					FortiONMobileServiceClient c = DaoUtil.GetClient ();

                    c.HasNewMessagesCompleted += (sender, e) =>
                    {
                       Device.BeginInvokeOnMainThread(() => {
                            if (e.Result)
                            {
                                DisplayAlert("Fortion", "Nova Notificação", "OK");
                                btnNotificacao.Text += StrNovaNotificacao;
                                LoadNotificacao(Util.GetToken());
                            }
                       });
                    };

                    if (Util.IsNetworkConnected())
                    {
                        c.HasNewMessagesAsync(Util.GetToken());
                    }

					if(Util.IsNetworkConnected())
						imgNetwork.Source = "drawable/Wifi_On.png";
					else
						imgNetwork.Source = "drawable/Wifi_Off.png";
					if(JaExibiuUmaVez){
                        DadosEmbarque e = Util.LoadDadosEmbarque();
						LoadDadosEmbarqueFromServer(e);
						ExibeDadosEmbarque(e);
					}
					JaExibiuUmaVez = true;
				}
				catch(Exception ex){
				}
			});
		}

        void LoadNotificacao(string token)
        {
            List<NotificacaoEntidade> listaNotificacao = new List<NotificacaoEntidade>();

            try
            {
                FortiONMobileServiceClient c = DaoUtil.GetClient();

                if (listaNotificacao == null || listaNotificacao.Count == 0)
                {

                    if (!Util.IsNetworkConnected())
                    {
                        DisplayAlert("Fortion", "NÃO HÁ CONEXÃO COM A INTERNET", "OK");
                        return;
                    }

                    c.GetDadosMensagemCompleted += (send, args) =>
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            foreach (DadosMensagem i in args.Result)
                            {
                                NotificacaoEntidade n = new NotificacaoEntidade();
                                n.Texto = i.Mensagem;
                                n.Titulo = i.NomeAutor;
                                n.Id = i.Id;
                                if (i.Id_Origem != null)
                                    n.Id_Origem = i.Id_Origem;
                                else
                                    n.Id_Origem = -99;
                                n.DataMensagem = i.DataMensagem;
                                n.Sequencia = i.Sequencia;
                                n.Autor = i.Autor;
                                switch (n.Autor)
                                {
                                    case (byte)0:
                                        n.ImageUri = "drawable/Computador.png";
                                        break;
                                    case (byte)1:
                                        n.ImageUri = "drawable/Computador.png";
                                        break;
                                    case (byte)2:
                                        n.ImageUri = "drawable/Pessoa.png";
                                        break;
                                    case (byte)3:
                                        n.ImageUri = "drawable/Volante.png";
                                        break;
                                    case (byte)4:
                                        n.ImageUri = "drawable/Pessoa.png";
                                        break;
                                }

                                listaNotificacao.Add(n);
                            }

                            Util.SaveNotificacao(listaNotificacao);
                        });
                    };

                    c.GetDadosMensagemAsync(token, true);
                }
            }
            catch (Exception ex)
            {
                DisplayAlert("Fortion", "OCORREU UM PROBLEMA AO BUSCAR NOVAS MENSAGENS: " + ex.Message, "OK");
            }
        }

		public void OnUpdateGps() {
			try{
				Device.BeginInvokeOnMainThread (() => {
					if(Gps.IsEnabled && Gps.Availability == GpsAvailability.Available)
						ImgGps.Source = "drawable/GPS_On.png";
					else
						ImgGps.Source = "drawable/GPS_Off.png";
				});
			}
			catch(Exception ex){
				DisplayAlert ("Fortion", "OCORREU UM PROBLEMA: " + ex.Message, "OK");
			}
		}
			
		public void Tick(){
			Device.BeginInvokeOnMainThread (() => {
				try{
					if(Util.IsNetworkConnected())
						imgNetwork.Source = "drawable/Wifi_On.png";
					else
						imgNetwork.Source = "drawable/Wifi_Off.png";
					if(!btnNotificacao.Text.Contains(StrNovaNotificacao)){
						FortiONMobileServiceClient c = DaoUtil.GetClient ();

                        c.HasNewMessagesCompleted += (sender, args) =>
                        {
                            if (args.Result)
                            {
                                DisplayAlert("Fortion", "Nova Notificação", "OK");
                                btnNotificacao.Text += StrNovaNotificacao;
                            }
                        };

                        c.HasNewMessagesAsync(Util.GetToken());

					}
                    
                    DadosEmbarque e = Util.LoadDadosEmbarque();
					LoadDadosEmbarqueFromServer(e);
					ExibeDadosEmbarque(e);
				}
				catch(Exception ex){
					//lblDestino.Text = "Tick OCORREU UM PROBLEMA: " + ex.Message;
				}
			});
		}

		public void TestePosicao(){
			Device.BeginInvokeOnMainThread (() => {
				try{
					DisplayAlert("Teste", Gps.Latitude.ToString(), "OK");
					lblOrigem.Text = Gps.Latitude.ToString();
					lblDestino.Text = Gps.Longitude.ToString();
				}
				catch(Exception ex){
					lblDestino.Text = "TestePosicao OCORREU UM PROBLEMA: " + ex.Message;
				}
			});
		}
	}
}

