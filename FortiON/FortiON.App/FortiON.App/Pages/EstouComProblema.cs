﻿using System;
using Xamarin.Forms;
using FortiON.App.DataAccess;
using FortiON.App.DataAccess.ServiceReference;


namespace FortiON.App
{
	public class EstouComProblema : ContentPage
	{
		Menu Menu = new Menu();

		public EstouComProblema()
		{
			try{
				this.Title = "ESTOU COM PROBLEMA";

				StackLayout layoutMain = new StackLayout {
					Orientation = StackOrientation.Vertical,
					BackgroundColor = Color.White,
					VerticalOptions = LayoutOptions.FillAndExpand
				};

				StackLayout layoutCima = new StackLayout {
					Orientation = StackOrientation.Vertical,
					BackgroundColor = Color.White,
					VerticalOptions = LayoutOptions.FillAndExpand,
					Padding = new Thickness (20, 40, 20, 20)
				};

				StackLayout layoutBaixo = new StackLayout {
					Orientation = StackOrientation.Horizontal,
					BackgroundColor = Color.White,
					VerticalOptions = LayoutOptions.End
				};

				Label lblMsg = new Label {
					Text = "Você tem certeza que deseja iniciar o acompanhamento da entrega desta carga? Após confirmação, um operador FortiON irá entrar em contato com o embarcador para informar os problemas com a entrega.",
					TextColor = Color.Black
				};

				Button btnSim = new Button
				{
					Text = String.Format("Sim"),
					BackgroundColor = Util.FortiOnBlue,
					TextColor = Color.White,
					BorderRadius = 0,
					VerticalOptions = LayoutOptions.EndAndExpand,
					HorizontalOptions = LayoutOptions.FillAndExpand,
				};
				btnSim.Clicked += (sender, args) =>
				{

                    //Device.BeginInvokeOnMainThread(() =>
                    //{
                        try
                        {
                            if (Util.IsNetworkConnected())
                            {
                                FortiONMobileServiceClient c = DaoUtil.GetClient();
                                DadosTransmissao dadosTransmissao = new DadosTransmissao();
                                dadosTransmissao.ComProblema = true;
                                dadosTransmissao.VeiculoLat = Gps.Latitude;
                                dadosTransmissao.VeiculoLng = Gps.Longitude;

                                c.InformarProblemaCompleted += (send, e) =>
                                {
                                    Device.BeginInvokeOnMainThread(() =>
                                    {
                                        if (e.Result)
                                        {
                                            DisplayAlert("Fortion", "MENSAGEM ENVIADA", "OK");
                                            Navigation.PushAsync(new Principal());
                                        }
                                        else
                                        {
                                            DisplayAlert("Fortion", "NÃO FOI POSSÍVEL ENVIAR A MENSAGEM", "OK");
                                        }
                                    });
                                };

                                c.InformarProblemaAsync(Util.GetToken(), dadosTransmissao);
                            }
                            else
                            {
                                DisplayAlert("Fortion", "NÃO HÁ CONEXÃO COM A INTERNET", "OK");
                            }
                        }
                        catch (Exception ex)
                        {
                            DisplayAlert("Fortion", "OCORREU UM PROBLEMA: " + ex.Message, "OK");
                        }
                   // });       
				};

				Button btnNao = new Button
				{
					Text = String.Format("Não"),
					BackgroundColor = Util.FortiOnBlue,
					TextColor = Color.White,
					BorderRadius = 0,
					VerticalOptions = LayoutOptions.EndAndExpand,
					HorizontalOptions = LayoutOptions.FillAndExpand
				};
				btnNao.Clicked += (sender, args) =>
				{
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        Navigation.PushAsync(new Principal());
                    });
				};

				Menu.CreateToolBar (this);

				btnSim.Image = (FileImageSource) ImageSource.FromFile ("drawable/Sim.png");
				btnNao.Image = (FileImageSource) ImageSource.FromFile ("drawable/Nao.png");;

				layoutCima.Children.Add (lblMsg);
				layoutBaixo.Children.Add (btnSim);
				layoutBaixo.Children.Add (btnNao);
				layoutMain.Children.Add (layoutCima);
				layoutMain.Children.Add (layoutBaixo);

				this.Content = layoutMain;
			}
			catch(Exception ex){
				DisplayAlert ("Fortion", "OCORREU UM PROBLEMA: " + ex.Message, "OK");
			}
		}	
	}
}

