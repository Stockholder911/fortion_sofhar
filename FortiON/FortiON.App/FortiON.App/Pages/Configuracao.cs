﻿using System;
using Xamarin.Forms;
using FortiON.App.DataAccess;
using System.ServiceModel;
using System.Threading.Tasks;
using FortiON.App.DataAccess.ServiceReference;

namespace FortiON.App
{
	public class Configuracao : ContentPage
	{
		Menu Menu = new Menu();
		Entry txtToken;
   
		public Configuracao()
		{

			try{

				this.Title = "CONFIGURAR";

				StackLayout layoutMain = new StackLayout {
					Orientation = StackOrientation.Vertical,
					BackgroundColor = Color.White,
					VerticalOptions = LayoutOptions.FillAndExpand
				};

				StackLayout layoutCima = new StackLayout {
					Orientation = StackOrientation.Vertical,
					BackgroundColor = Color.White,
					VerticalOptions = LayoutOptions.FillAndExpand,
					Padding = new Thickness (20, 40, 20, 20)
				};

				StackLayout layoutBaixo = new StackLayout {
					Orientation = StackOrientation.Vertical,
					BackgroundColor = Color.White,
					VerticalOptions = LayoutOptions.End
				};

				Label lblMsg = new Label {
					Text = "Bem vindo ao aplicativo Fortion. Insira seu TOKEN e clique em \"Salvar\".\nCaso você não tenha ou não se lembre do seu TOKEN, entre em contato com a Fortion no telefone: 41 - 99471560",
					TextColor = Color.Black
				};

				txtToken = new Entry{
					TextColor = Color.Black,
					Text = Util.GetToken()
				};

				Button btnSalvar = new Button
				{
					Text = String.Format("Salvar           "),
					BackgroundColor = Util.FortiOnBlue,
					TextColor = Color.White,
					BorderRadius = 0,
					VerticalOptions = LayoutOptions.EndAndExpand
				};

				btnSalvar.Clicked += (sender, args) =>
                {
                      // Device.BeginInvokeOnMainThread(() =>
                       //  {
                           try
                           {
                               if (Util.IsNetworkConnected())
                               {
                                   FortiONMobileServiceClient c = DaoUtil.GetClient();
                                   c.IsTokenValidCompleted += isTokenValidCompleted;
                                   string token = txtToken.Text;
                                   c.IsTokenValidAsync(token);
                               }
                               else
                               {
                                   DisplayAlert("Fortion", "NÃO HÁ CONEXÃO COM A INTERNET", "OK");
                               }
                           }
                           catch (Exception ex)
                           {
                               DisplayAlert("Fortion", "OCORREU UM PROBLEMA: " + ex.Message, "OK");
                           }
                     //  });
                  };
              
                Menu.CreateToolBar (this);

				txtToken.Focus ();

				btnSalvar.Image = (FileImageSource) ImageSource.FromFile ("drawable/Salvar.png");;

				layoutCima.Children.Add (lblMsg);
				layoutCima.Children.Add (txtToken);
				layoutBaixo.Children.Add (btnSalvar);
				layoutMain.Children.Add (layoutCima);
				layoutMain.Children.Add (layoutBaixo);

				this.Content = layoutMain;
			}
			catch(Exception ex){
				DisplayAlert ("Fortion", "OCORREU UM PROBLEMA: " + ex.Message + " " + ex.StackTrace, "OK");
			}
		}

        private void isTokenValidCompleted(object sender, IsTokenValidCompletedEventArgs e)
        {
           Device.BeginInvokeOnMainThread(() =>
           {
                if (e.Result)
                {
                    //@TH - gravar o token e redirecionar para o main
                    Util.SaveToken(txtToken.Text);
                    DisplayAlert("Fortion", "CONFIGURAÇÃO REALIZADA", "OK");
                    Navigation.PushAsync(new Principal());
                }
                else
                {
                    //@TH - dar mensagem de token inválido
                    DisplayAlert("Fortion", "TOKEN INVÁLIDO", "OK");
                }
           });
        }
	}
}

