﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using FortiON.App.DataAccess;
using System.ServiceModel;
using System.Linq;
using FortiON.App.DataAccess.ServiceReference;

namespace FortiON.App
{
	public class Notificacao : ContentPage
	{
		Menu Menu = new Menu();

		NotificacaoEntidade selected;
        ListView listView;

		public Notificacao()
		{
			try{
				this.Title = "NOTIFICAÇÕES";

				ScrollView layoutScroll = new ScrollView  {
					Orientation = ScrollOrientation.Vertical,
					BackgroundColor = Color.White,
					VerticalOptions = LayoutOptions.FillAndExpand
				};

				StackLayout layoutMain = new StackLayout {
					Orientation = StackOrientation.Vertical,
					BackgroundColor = Color.White,
					VerticalOptions = LayoutOptions.FillAndExpand
				};

				StackLayout layoutCima = new StackLayout {
					Orientation = StackOrientation.Vertical,
					BackgroundColor = Color.White,
					VerticalOptions = LayoutOptions.FillAndExpand,
					Padding = new Thickness(0, 4, 0, 0)
				};

				StackLayout layoutBaixo = new StackLayout {
					Orientation = StackOrientation.Vertical,
					BackgroundColor = Color.White,
					VerticalOptions = LayoutOptions.End
				};

				Button btnResponder = new Button
				{
					Text = String.Format("Responder      "),
					BackgroundColor = Util.FortiOnBlue,
					TextColor = Color.White,
					BorderRadius = 0,
					VerticalOptions = LayoutOptions.EndAndExpand
				};

				btnResponder.Image = (FileImageSource) ImageSource.FromFile ("drawable/ResponderNotificacao.png");

				Menu.CreateToolBar (this);

                listView = new ListView
				{
					RowHeight = 110
				};

                List<NotificacaoEntidade> listNotificacaoEntidade = Util.LoadNotificacao();
                if (listNotificacaoEntidade == null)
                {
                    LoadNotificacao(Util.GetToken());
                }
                else
                {
                    listView.ItemsSource = listNotificacaoEntidade;
                }

                listView.ItemTemplate = new DataTemplate(typeof(NotificacaoCell));
				listView.ItemSelected += (sender, args) =>
				{
					// Deselect previous
					if ( selected != null )
					{
						selected.SetColors( false );
					}

					// Select new
					selected = (listView.SelectedItem as NotificacaoEntidade);
					selected.SetColors( true );
				};

				btnResponder.Clicked += (sender, args) =>
				{
                    //Device.BeginInvokeOnMainThread(() =>
                    //{
                        if (listView.SelectedItem != null)
                        {
                            NotificacaoEntidade e = (NotificacaoEntidade)listView.SelectedItem;
                            if (e.Autor != (byte)0 && e.Autor != (byte)3)
                            {
                                Util.NotificacaoAtiva = e;
                                Navigation.PushAsync(new ResponderNotificacao());
                            }
                            else
                                DisplayAlert("Fortion", "NÃO É POSSÍVEL RESPONDER NOTIFICAÇÃO DO SISTEMA OU MOTORISTA", "OK");
                        }
                        else
                        {
                            DisplayAlert("Fortion", "SELECIONE UMA NOTIFICAÇÃO", "OK");
                        }
                    //});
				};

				Button btnAtualizar = new Button
				{
					Text = String.Format("Atualizar"),
					BackgroundColor = Util.FortiOnBlue,
					TextColor = Color.White,
					BorderRadius = 0,
					VerticalOptions = LayoutOptions.EndAndExpand
				};

				btnAtualizar.Clicked += (sender, args) =>
				{
                    LoadNotificacao(Util.GetToken());
				};

				layoutScroll.Content = layoutCima;

				layoutCima.Children.Add (listView);
				layoutBaixo.Children.Add (btnAtualizar);
				layoutBaixo.Children.Add (btnResponder);
				layoutMain.Children.Add (layoutScroll);
				layoutMain.Children.Add (layoutBaixo);

				this.Content = layoutMain;
			}
			catch(Exception ex){
				DisplayAlert ("Fortion", "OCORREU UM PROBLEMA: " + ex.Message, "OK");
			}
		}


        void LoadNotificacao(string token)
        {
            List<NotificacaoEntidade> listaNotificacao = new List<NotificacaoEntidade>();

            try
            {
                FortiONMobileServiceClient c = DaoUtil.GetClient();

                if (listaNotificacao == null || listaNotificacao.Count == 0)
                {

                    if (!Util.IsNetworkConnected())
                    {
                        DisplayAlert("Fortion", "NÃO HÁ CONEXÃO COM A INTERNET", "OK");
                        return;
                    }

                    c.GetDadosMensagemCompleted += (send, args) =>
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            foreach (DadosMensagem i in args.Result)
                            {
                                NotificacaoEntidade n = new NotificacaoEntidade();
                                n.Texto = i.Mensagem;
                                n.Titulo = i.NomeAutor;
                                n.Id = i.Id;
                                if (i.Id_Origem != null)
                                    n.Id_Origem = i.Id_Origem;
                                else
                                    n.Id_Origem = -99;
                                n.DataMensagem = i.DataMensagem;
                                n.Sequencia = i.Sequencia;
                                n.Autor = i.Autor;
                                switch (n.Autor)
                                {
                                    case (byte)0:
                                        n.ImageUri = "drawable/Computador.png";
                                        break;
                                    case (byte)1:
                                        n.ImageUri = "drawable/Computador.png";
                                        break;
                                    case (byte)2:
                                        n.ImageUri = "drawable/Pessoa.png";
                                        break;
                                    case (byte)3:
                                        n.ImageUri = "drawable/Volante.png";
                                        break;
                                    case (byte)4:
                                        n.ImageUri = "drawable/Pessoa.png";
                                        break;
                                }
                                listaNotificacao.Add(n);
                            }

                            listView.ItemsSource = listaNotificacao;
                            Util.SaveNotificacao(listaNotificacao);
                        });
                    };

                    c.GetDadosMensagemAsync(token, true);
                }
            }
            catch (Exception ex)
            {
                DisplayAlert("Fortion", "OCORREU UM PROBLEMA AO BUSCAR NOVAS MENSAGENS: " + ex.Message, "OK");
            }
        }
	}
}

