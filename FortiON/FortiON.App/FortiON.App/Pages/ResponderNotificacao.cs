﻿using System;
using Xamarin.Forms;
using FortiON.App.DataAccess;
using FortiON.App.DataAccess.ServiceReference;

namespace FortiON.App
{
	public class ResponderNotificacao : ContentPage
	{
		Menu Menu = new Menu();

		bool IsJaRespondeu = false;

		public ResponderNotificacao()
		{
			try{
				this.Title = "RESPONDER";

				StackLayout layoutMain = new StackLayout {
					Orientation = StackOrientation.Vertical,
					BackgroundColor = Color.White,
					VerticalOptions = LayoutOptions.FillAndExpand
				};

				StackLayout layoutCima = new StackLayout {
					Orientation = StackOrientation.Vertical,
					BackgroundColor = Color.White,
					VerticalOptions = LayoutOptions.FillAndExpand,
					Padding = new Thickness (0, 20, 0, 0)
				};

				StackLayout layoutMeio = new StackLayout {
					Orientation = StackOrientation.Vertical,
					BackgroundColor = Color.White,
					VerticalOptions = LayoutOptions.End
				};

				StackLayout layoutBaixo = new StackLayout {
					Orientation = StackOrientation.Vertical,
					BackgroundColor = Color.White,
					VerticalOptions = LayoutOptions.End
				};

				Editor edtResponder = new Editor {
					BackgroundColor = Util.FortionGray,
					VerticalOptions = LayoutOptions.FillAndExpand
				};

				Switch schDesvio = new Switch {
					HorizontalOptions = LayoutOptions.End
				};

				Switch schProblema = new Switch {
					HorizontalOptions = LayoutOptions.End
				};

				Label lblDesvio = new Label {
					Text = "Desvio",
					HorizontalOptions = LayoutOptions.EndAndExpand,
					VerticalOptions = LayoutOptions.Center
				};

				Label lblProblema = new Label {
					Text = "Problema",
					HorizontalOptions = LayoutOptions.EndAndExpand,
					VerticalOptions = LayoutOptions.Center
				};

				ActivityIndicator loading = new ActivityIndicator();
				loading.IsRunning = false;

				StackLayout layoutCheckBox = new StackLayout {
					Orientation = StackOrientation.Vertical,
					Padding = new Thickness(20, 20, 20, 20)
				};

				StackLayout layoutCheckBoxDesvio = new StackLayout {
					Orientation = StackOrientation.Horizontal,
					Padding = new Thickness(0, 0, 0, 10)
				};

				StackLayout layoutCheckBoxProblema = new StackLayout {
					Orientation = StackOrientation.Horizontal,
				};

				layoutCheckBoxDesvio.Children.Add (lblDesvio);
				layoutCheckBoxDesvio.Children.Add (schDesvio);

				layoutCheckBoxProblema.Children.Add (lblProblema);
				layoutCheckBoxProblema.Children.Add (schProblema);

				layoutCheckBox.Children.Add (layoutCheckBoxDesvio);
				layoutCheckBox.Children.Add (layoutCheckBoxProblema);

				Button btnEnviar = new Button
				{
					Text = String.Format("Enviar Resposta"),
					BackgroundColor = Util.FortiOnBlue,
					TextColor = Color.White,
					BorderRadius = 0,
					VerticalOptions = LayoutOptions.EndAndExpand
				};
				btnEnviar.Clicked += (sender, args) =>
				{
					try{
						if(IsJaRespondeu)
							return;
						if(edtResponder.Text.Length < 2){
							DisplayAlert("Fortion", "RESPOSTA MUITA CURTA OU EM BRANCO", "OK");
							return;
						}
                        if (Util.IsNetworkConnected())
                        {
                            btnEnviar.IsEnabled = false;
                            loading.IsRunning = true;
                            FortiONMobileServiceClient c = DaoUtil.GetClient();
                            DadosRespostaMensagem r = new DadosRespostaMensagem();
                            DadosTransmissao d = new DadosTransmissao();
                            r.Id_Origem = Util.NotificacaoAtiva.Id;
                            r.Mensagem = edtResponder.Text;
                            d.EmDesvio = schDesvio.IsToggled;
                            d.ComProblema = schProblema.IsToggled;
                            //Util.AtualizaGps();
                            d.VeiculoLat = Gps.Latitude;
                            d.VeiculoLng = Gps.Longitude;

                            c.ResponderMensagemAsync(Util.GetToken(), r, d);

                            c.ResponderMensagemCompleted += (send, e) =>
                            {
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    if (e.Result)
                                    {
                                        IsJaRespondeu = true;
                                        DisplayAlert("Fortion", "RESPOSTA ENVIADA", "OK");
                                        Navigation.PushAsync(new Notificacao());
                                    }
                                    else
                                    {
                                        DisplayAlert("Fortion", "NÃO FOI POSSÍVEL ENVIAR RESPOSTA", "OK");
                                    }
                                });
                            };
                        }
                        else
                        {
                            DisplayAlert("Fortion", "NÃO HÁ CONEXÃO COM A INTERNET", "OK");
                        }
					}
					catch(Exception ex){
						DisplayAlert ("Fortion", "OCORREU UM PROBLEMA: " + ex.Message, "OK");
					}
					finally{
						btnEnviar.IsEnabled = true;
						loading.IsRunning = false;
					}
				};

				Menu.CreateToolBar (this);

				edtResponder.Focus ();

				btnEnviar.Image = (FileImageSource) ImageSource.FromFile ("drawable/Responder.png");;

				layoutCima.Children.Add (edtResponder);
				layoutMeio.Children.Add (layoutCheckBox);
				layoutBaixo.Children.Add (btnEnviar);
				layoutMain.Children.Add (layoutCima);
				layoutMain.Children.Add (layoutMeio);
				layoutMain.Children.Add (loading);
				layoutMain.Children.Add (layoutBaixo);

				this.Content = layoutMain;
			}
			catch(Exception ex){
				DisplayAlert ("Fortion", "OCORREU UM PROBLEMA: " + ex.Message, "OK");
			}
		}
	}
}

