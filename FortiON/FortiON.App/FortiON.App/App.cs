﻿using System;
using Xamarin.Forms;
using FortiON.App.DataAccess;

namespace FortiON.App
{
    public class App : Application
    {
        public App()
        {
            Util.GetToken();
            Util.LoadDadosEmbarque();

            var navigon = new NavigationPage(new Principal())
            {
                BarBackgroundColor = Util.FortiOnBlue,
                BarTextColor = Color.White
            };
            MainPage = navigon;
        }

        protected override void OnResume()
        {
            //Console.WriteLine("OnResume");
            base.OnResume();
        }

        protected override void OnSleep()
        {
            //Console.WriteLine("OnSleep");
            base.OnSleep();
        }

        protected override void OnStart()
        {
            //Console.WriteLine("OnStart");
            base.OnStart();
        }
    }
}

