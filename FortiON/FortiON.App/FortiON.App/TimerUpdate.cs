﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using FortiON.App.DataAccess;

namespace FortiON.App
{
	public class TimerUpdate
	{
		public static ITimerUpdate Listener;

		public static void Tick(){
			try{
				if(Listener != null)
					Listener.Tick();
			}
			catch(Exception ex){
			}
		}
	}

	public interface ITimerUpdate{
		void Tick();
	}
}

