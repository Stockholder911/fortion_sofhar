﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace FortiON.App
{
	public class Menu : ContentPage
	{
		public void CreateToolBar(ContentPage page)//IList<ToolbarItem> toolBarItens
		{

			var cmdHome = new Command (() => page.Navigation.PushAsync(new Principal()));
			var home = new ToolbarItem
			{
				//Icon = "settings32.png",
				Text = "Home",
				Command = cmdHome
			};

			var cmdNotificacao = new Command (() => page.Navigation.PushAsync(new Notificacao()));
			var notificacao = new ToolbarItem
			{
				//Icon = "settings32.png",
				Text = "Notificações",
				Command = cmdNotificacao
			};

			var cmdInformacaoEntrega = new Command (() => page.Navigation.PushAsync(new Confirmacao()));
			var informacaoEntrega = new ToolbarItem
			{
				//Icon = "settings32.png",
				Text = "Informar Entrega",
				Command = cmdInformacaoEntrega
			};

			var cmdCfg = new Command (() => page.Navigation.PushAsync(new Configuracao()));
			var cfg = new ToolbarItem
			{
				//Icon = "settings32.png",
				Text = "Configuração",
				Command = cmdCfg
			};

			home.Order = ToolbarItemOrder.Secondary;
			notificacao.Order = ToolbarItemOrder.Secondary;
			informacaoEntrega.Order = ToolbarItemOrder.Secondary;
			cfg.Order = ToolbarItemOrder.Secondary;

			page.ToolbarItems.Add (home);
			page.ToolbarItems.Add (notificacao);
			page.ToolbarItems.Add (informacaoEntrega);
			page.ToolbarItems.Add (cfg);
		}
	}
}

