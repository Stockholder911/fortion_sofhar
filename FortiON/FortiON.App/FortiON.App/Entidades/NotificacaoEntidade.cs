﻿using System;
using Xamarin.Forms;
using System.ComponentModel;

namespace FortiON.App
{
	public class NotificacaoEntidade : INotifyPropertyChanged
	{
		public string ImageUri { get; set; }
		public string Titulo { get; set; }
		public string Texto { get; set; }
		public int Id { get; set; }
		public int Id_Origem { get; set; }
		public DateTime DataMensagem { get; set; }
		public short Sequencia { get; set; }
		public byte Autor { get; set; }

		public NotificacaoEntidade ()
		{

		}

		public event PropertyChangedEventHandler PropertyChanged;

		private Color _backgroundColor;

		public Color BackgroundColor 
		{ 
			get { return _backgroundColor; } 
			set 
			{ 
				_backgroundColor = value; 

				if ( PropertyChanged != null )
				{
					PropertyChanged( this, new PropertyChangedEventArgs( "BackgroundColor" ) );
				}
			}
		}

		public void SetColors( bool isSelected )
		{
			if(Util.FortiOnItemNoSelected == Color.Black)
				Util.FortiOnItemNoSelected = BackgroundColor;

			if ( isSelected )
			{
				BackgroundColor = Util.FortiOnItemSelected;;
			}
			else
			{
				BackgroundColor = Util.FortiOnItemNoSelected; 
			}
		}
	}
}

