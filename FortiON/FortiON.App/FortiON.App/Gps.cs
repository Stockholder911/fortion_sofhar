﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using FortiON.App.DataAccess;
using FortiON.App.DataAccess.ServiceReference;

namespace FortiON.App
{
	public class Gps
	{
		public static bool IsEnabled = true;
		public static GpsAvailability Availability;
		public static IGps Listener;

		public static double Longitude;
		public static double Latitude;

		public static float EnviaPosicaoIntervalo = 3;
		public static DateTime EnviaPosicaoTempoAtual = DateTime.Now;

		public static void OnUpdateGps(){
			try{
				if (Listener != null){
					Listener.OnUpdateGps ();
					//Listener.TestePosicao ();
				}
				int tempoPassado = (DateTime.Now - EnviaPosicaoTempoAtual).Minutes;
				if (tempoPassado >= EnviaPosicaoIntervalo) {
					EnviaPosicaoTempoAtual = DateTime.Now;
					FortiONMobileServiceClient c = DaoUtil.GetClient (new TimeSpan(0, 0, 15));
					DadosTransmissao dadosTransmissao = new DadosTransmissao ();
					dadosTransmissao.VeiculoLat = Gps.Latitude;
					dadosTransmissao.VeiculoLng = Gps.Longitude;
                    c.InformarPosicaoAsync(Util.GetToken(), dadosTransmissao);
				}
			}
			catch(Exception ex){
			}
		}
	}

	public enum GpsAvailability{
		Available,
		OutOfService,
		TemporarilyUnavailable
	}

	public interface IGps{
		void OnUpdateGps();
		void TestePosicao();
	}
}

