﻿using System;

namespace FortiON.App
{
	public interface INetwork{
		bool IsNetworkConnected ();
	}
}

