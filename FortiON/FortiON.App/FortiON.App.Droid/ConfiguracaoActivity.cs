﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace FortiON.App.Android
{
	[Activity (Label = "Configurar" /*, ParentActivity = typeof(MainActivity) */)]
    [MetaData("android.support.PARENT_ACTIVITY", Value = "FortiON.App.Android.MainActivity")]	
	public class ConfiguracaoActivity : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
            
			// Create your application here
			//this.Title = "Configurar";
			//getActionBar().setIcon(R.drawable.my_icon);
			//ActionBar.SetHomeButtonEnabled(true);
			SetContentView (Resource.Layout.Configuracao);
			ActionBar.SetDisplayHomeAsUpEnabled(true);

			Button btnSalvar = FindViewById<Button> (Resource.Id.btnCfgSalvar);

			btnSalvar.Click += btnSalvarClick;
		}

		void btnSalvarClick (object sender, EventArgs ea) {
			//DisplayAlert ("Alert", "You have been alerted", "OK");
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.ItemId)
			{
			case Resource.Id.Configuracao:
				// place holder for creating new poi
				StartActivity (typeof(ConfiguracaoActivity));
				return true;

			default :
				return base.OnOptionsItemSelected(item);
			}
		}
	}
}

