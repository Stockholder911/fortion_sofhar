﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

using Android.Net;
using Android.Locations;
using System.Threading;

namespace FortiON.App.Android
{
	[Activity (Label = "Fortion", Icon = "@drawable/FortiOnIcon", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : FormsApplicationActivity, ILocationListener
	{
		LocationManager locMgr;
		public static Context Context;

		static int TimeoutTimer = 60 * 1000;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate(bundle);

			Forms.Init(this, bundle);

			Network.Context = this;
			GpsUpdate.Context = this;

			locMgr = GetSystemService (Context.LocationService) as LocationManager;
			string provider = LocationManager.GpsProvider;

			locMgr.RequestLocationUpdates (provider, 60000, 1, this);
			if(locMgr.IsProviderEnabled(provider))
			{
				//locMgr.RequestLocationUpdates (provider, 600000, 1, this);
				//Gps.IsEnabled = locMgr.IsProviderEnabled (LocationManager.GpsProvider);
			} 
			else 
			{
				Gps.IsEnabled = false;
				//Log.Info(tag, Provider + " is not available. Does the device have location services enabled?");
			}

			LoadApplication(new App());

			TimerCallback timerDelegate = new TimerCallback(Tick);
			Timer timer = new Timer (timerDelegate, null, TimeoutTimer, TimeoutTimer);
		}

		static void Tick(Object state) {
			TimerUpdate.Tick ();
		}

		public void OnProviderEnabled (string provider)
		{
			Gps.IsEnabled = true;
			Gps.OnUpdateGps ();
		}

		public void OnProviderDisabled (string provider)
		{
			Gps.IsEnabled = false;
			Gps.OnUpdateGps ();
		}

		public void OnStatusChanged (string provider, Availability status, Bundle extras)
		{
			switch (status) {
			case Availability.Available:
				Gps.Availability = GpsAvailability.Available;
				break;
			case Availability.OutOfService:
				Gps.Availability = GpsAvailability.OutOfService;
				break;
			case Availability.TemporarilyUnavailable:
				Gps.Availability = GpsAvailability.TemporarilyUnavailable;
				break;
			}
			Gps.OnUpdateGps ();
		}

		public void OnLocationChanged (Location location)
		{
			Gps.Latitude = location.Latitude;
			Gps.Longitude = location.Longitude;
			Gps.OnUpdateGps ();
		}
	}
}

