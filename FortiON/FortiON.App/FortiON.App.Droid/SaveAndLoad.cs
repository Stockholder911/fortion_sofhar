﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using System.IO;

[assembly: Dependency (typeof (FortiON.App.Android.SaveAndLoad))]
namespace FortiON.App.Android
{
	public class SaveAndLoad : ISaveAndLoad {
		public void SaveText (string filename, string text) {
			var documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
			var filePath = Path.Combine (documentsPath, filename);
			System.IO.File.Delete (filePath);
			System.IO.File.WriteAllText (filePath, text);
		}
		public string LoadText (string filename) {
			var documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
			var filePath = Path.Combine (documentsPath, filename);
			if (File.Exists (filePath))
				return System.IO.File.ReadAllText (filePath);
			else
				return string.Empty;
		}
	}
}

