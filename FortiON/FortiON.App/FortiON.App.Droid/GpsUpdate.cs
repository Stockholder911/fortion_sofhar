﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

using Android.Net;
using Android.Locations;

[assembly: Dependency (typeof (FortiON.App.Android.GpsUpdate))]
namespace FortiON.App.Android
{
	public class GpsUpdate : IGpsUpdate
	{
		public static Context Context;

		public void AtualizaGps(){
		
			LocationManager locMgr = Context.GetSystemService (Context.LocationService) as LocationManager;
			string provider = LocationManager.GpsProvider;
			Location location = locMgr.GetLastKnownLocation (provider);
			GpsStatus status = locMgr.GetGpsStatus (null);
			Gps.Latitude = location.Latitude;
			Gps.Longitude = location.Longitude;
		}
	}
}

