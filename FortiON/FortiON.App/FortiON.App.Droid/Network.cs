﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

using Android.Net;
using Android.Locations;

[assembly: Dependency (typeof (FortiON.App.Android.Network))]
namespace FortiON.App.Android
{
	public class Network : INetwork
	{
		public static Context Context;

		public bool IsNetworkConnected(){

			ConnectivityManager cm = (ConnectivityManager) Context.GetSystemService(Context.ConnectivityService);
			if (cm != null && cm.ActiveNetworkInfo != null)
				return cm.ActiveNetworkInfo.IsConnected;
			else
				return false;
		}
	}
}

