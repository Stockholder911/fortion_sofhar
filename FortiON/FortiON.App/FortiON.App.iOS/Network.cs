﻿using System;
using Xamarin.Forms;

[assembly: Dependency(typeof(FortiON.App.iOS.Network))]
namespace FortiON.App.iOS
{
	public class Network : INetwork
	{
		public bool IsNetworkConnected(){

            NetworkStatus internetStatus = Reachability.InternetConnectionStatus();

            return !internetStatus.Equals(NetworkStatus.NotReachable);

            //return Reachability.IsHostReachable("http://google.com");
		}
	}
}

