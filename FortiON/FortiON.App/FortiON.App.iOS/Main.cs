﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;
using CoreLocation;
using System.Threading;

namespace FortiON.App.iOS
{
    public class Application
    {
        static CLLocationManager LocMgr;
        static int TimerTimeout = 60 * 1000;

        // This is the main entry point of the application.
        static void Main(string[] args)
        {
            UpdateGpsStatus();

            LocMgr = new CLLocationManager();
            LocMgr.DesiredAccuracy = 1;

            if (UIDevice.CurrentDevice.CheckSystemVersion(6, 0))
            {
                LocMgr.LocationsUpdated += (object sender, CLLocationsUpdatedEventArgs e) =>
                {
                    try
                    {
                        Gps.Latitude = e.Locations[0].Coordinate.Latitude;
                        Gps.Longitude = e.Locations[0].Coordinate.Longitude;
                    }
                    catch (Exception ex) { }
                };
            }
            else
            {
                // this will be called pre-iOS 6
                LocMgr.UpdatedLocation += (object sender, CLLocationUpdatedEventArgs e) =>
                {
                    try
                    {
                        Gps.Latitude = e.NewLocation.Coordinate.Latitude;
                        Gps.Longitude = e.NewLocation.Coordinate.Longitude;
                    }
                    catch (Exception ex) { }
                };
            }

            LocMgr.StartUpdatingLocation();

            TimerCallback timerDelegate = new TimerCallback(Tick);
            Timer timer = new Timer(timerDelegate, null, TimerTimeout, TimerTimeout);

            // if you want to use a different Application Delegate class from "AppDelegate"
            // you can specify it here.
            UIApplication.Main(args, null, "AppDelegate");
        }

        static void Tick(Object state)
        {
            UpdateGpsStatus();
            TimerUpdate.Tick();
        }

        static void UpdateGpsStatus()
        {
            if (CLLocationManager.LocationServicesEnabled)
            {
                Gps.IsEnabled = true;
                Gps.Availability = GpsAvailability.Available;
            }
            else 
            {
                Gps.IsEnabled = false;
                Gps.Availability = GpsAvailability.TemporarilyUnavailable;
            }
        }
    }
}
