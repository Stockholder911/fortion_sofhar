﻿using System;
using Xamarin.Forms;
using System.IO;

[assembly: Dependency(typeof(FortiON.App.iOS.SaveAndLoad))]
namespace FortiON.App.iOS
{
	public class SaveAndLoad : ISaveAndLoad {
		public void SaveText (string filename, string text) {
			var documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
			var filePath = Path.Combine (documentsPath, filename);
			System.IO.File.Delete (filePath);
            System.IO.File.CreateText(filePath).Write(filePath, text);
		}
		public string LoadText (string filename) {
			var documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
			var filePath = Path.Combine (documentsPath, filename);
			if (File.Exists (filePath))
				return System.IO.File.OpenText(filePath).ReadToEnd();
			else
				return string.Empty;
		}
	}
}

